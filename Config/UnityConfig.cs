using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc5;

using RegistryHopRestService.DAL.Application.Account.Confirm.Repository;
using RegistryHopRestService.DAL.Application.Account.DAO;

using RegistryHopRestService.BLL.Application.Account.Service;

namespace RegistryHopRestService.Core.Config
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();

            //
            //  Data
            //
            container.RegisterType<IAccountConfirmRepository, AccountConfirmRepository>();
            container.RegisterType<IAccountDAO, AccountDAO>();

            //
            //  Business
            //
            container.RegisterType<IAccountCreateService, AccountCreateService>();
            
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}